lib.hreflang = HMENU
lib.hreflang {
    special = language
    special.value = 1,0
    special.normalWhenNoLanguage = 0
    1 = TMENU
    1 {
        NO = 1
        NO {
            stdWrap.cObject = TEXT
            stdWrap.cObject {
                value =  en || de
            }
            linkWrap = <link rel="alternate" hreflang="|
            doNotLinkIt = 1
            after.cObject = TEXT
            after.cObject {
                stdWrap {
                    # wrap = " href="{$const.global.siteProtocol}://{$const.global.siteUrl}|">
                    wrap = " href="https://www.domain.tld|">
                    typolink {
                        parameter.data = page:uid
                        additionalParams = &L=1 || &L=0
                        addQueryString = 1
                        addQueryString.exclude = L,id,cHash,no_cache
                        returnLast = url
                    }
                }
            }
            allStdWrap.noTrimWrap (
|
||
            )
        }
        CUR = 1
        CUR {
            doNotShowLink = 1
            doNotLinkIt = 1
        }

        USERDEF1 = 1
        USERDEF1 {
            doNotShowLink = 1
            doNotLinkIt = 1
        }
    }
}