## Background image HTML
lib.backgroundimage = IMG_RESOURCE
lib.backgroundimage {
    file {
        import.data = levelmedia:-1, slide
        treatIdAsReference = 1
        import.listNum = rand
    }
    stdWrap {
        wrap = <div class="backStretch" data-src="|"></div>
    }
}
